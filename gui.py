import os
from flaskwebgui import FlaskUI

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

app = FlaskUI(**{
    'server': "django",
    'browser_path': os.path.join(BASE_DIR, "chrwin64", "bin", "chrome.exe")
})
app.run()
