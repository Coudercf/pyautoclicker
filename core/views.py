from django.http import JsonResponse
from django.shortcuts import render


def index(request):
    return render(request, "core/index.html")


def register(request):
    
    return JsonResponse({"data": "ok"})
