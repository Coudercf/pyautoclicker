import os
import time
import pickle
import pyautogui
from threading import Thread
from pynput import mouse, keyboard
from core.services import Click, Input, Scroll


class Auto(Thread):

    def __init__(self, name):
        Thread.__init__(self)
        self.name = name
        self.session = None
        self.mouse_controller = mouse.Controller()
        self.keyboard_controller = keyboard.Controller()

    def run(self):
        self.load_session()
        for action in self.session:
            if type(action) == Click:
                pyautogui.click(action.x, action.y)
                time.sleep(1)
            if type(action) == Scroll:
                pyautogui.scroll(action.direction)
            if type(action) == Input:
                self.keyboard_controller.press(action.key)
            if type(action) == float:
                time.sleep(action)
        self.finish()

    def finish(self):
        pass

    def load_session(self):
        if os.path.exists('./save/%s.ses' % self.name):
            with open('./save/%s.ses' % self.name, 'rb') as session_save:
                self.session = pickle.load(session_save)
        else:
            self._stop()
