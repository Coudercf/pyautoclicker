from django.db import models

# Create your models here.
class Session(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Move(models.Model):
    session = models.ForeignKey('core.Session', models.CASCADE)
    index = models.IntegerField()


class Click(Move):
    x = models.IntegerField()
    y = models.IntegerField()

    def __str__(self):
        return '[%s][%s] click(%s, %s)' % (self.session.name, self.index, self.x, self.y)


class Input(Move):
    key = models.CharField(max_length=255)

    def __str__(self):
        return '[%s][%s] input(%s)' % (self.session.name, self.index, self.key)


class Scroll(Move):
    direction = models.IntegerField()

    def __str__(self):
        return '[%s][%s] scroll(%s)' % (self.session.name, self.index, self.direction)
